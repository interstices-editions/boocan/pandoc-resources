---
title: L\'riture la la
---

# L\'écriture inclusive, ce n\'est pas que le point médian 

## Partie 1 sur 4

Il y a quelque temps, j'ai commencé à m'intéresser à l'écriture
inclusive, car j'entendais tout et son contraire à ce sujet. J'ai
commencé à lire des articles, des livres, regarder des vidéos, etc. Ma
conclusion a été "ce sujet est passionnant". Mais, j\'ai surtout
découvert l'impact sociétal de l'écriture inclusive.

L'objectif de cet article est de faire un condensé de mes lectures sur
pourquoi ce sujet est important avec une rétrospective historique, les
impacts, comment pratiquer une écriture inclusive et surtout revenir sur
plusieurs idées reçues.

Pour commencer, je vous propose 2 définitions de Raphaël Haddad qui a
beaucoup publié sur le sujet :

L'écriture inclusive n'est pas woke. C'est une tradition française.
Reféminisation des noms de métiers, convention de proximité, double
flexion : elle dépasse largement la question du point médian *L'écriture
inclusive, et si on s'y mettait* aux éditions le Robert).

Cette première citation est intéressante, car elle pointe du doigt le
premier apriori que l'on peut avoir en indiquant les différents moyens
mis à disposition. Il ne s\'agit pas de déformer l'écriture, mais
d'utiliser toute sa richesse.

La seconde définition : L'écriture inclusive peut se définir simplement
comme : un ensemble d'attentions éditoriales qui permettent d'assurer
une égalité de représentations entre les femmes et les hommes (*Manuel
d'écriture inclusive*, Raphaël Haddad, 2016) explicite clairement
l'objectif d'user d'une écriture inclusive : être un des outils de
l'égalité.

### Petit historique

Je vous propose un retour dans le passé, et pas n'importe où, au Moyen-
ge, au XIIIe siècle. À cette époque, le registre d'impôt médiéval dresse
une série de métiers parmi lesquels on trouve mairesse, archière,
cervoisière, portière ou fusicienne ("femme qui exerce la médecine"). Il
reconnaît les chevaleresses, les gouverneresses ou les médecines.

À cette même époque, l'accord de proximité est admis, c'est à dire,
qu'on accorde en genre l'adjectif par rapport au terme le plus
géographiquement proche. On peut se demander aujourd'hui pourquoi on
accepte "des chants et danses bretonnes" et pas \"des hommes et des
femmes bretonnes" ? Enfin, le "e" final n'est pas muet.

Et là, patatra ! En 1634, Richelieu crée l'Académie française. Pour
celleux qui ne la connaissent pas, il s\'agit d'une institution qui a
pour objectif de donner des règles certaines à notre langue et à la
rendre pure, éloquente et capable de traiter les arts et les sciences.
Si vous souhaitez en savoir plus, je vous invite à aller regarder
. J'en profite pour vous donner quelques statistiques sur cette
institution :

-   1980 : Marguerite Yourcenar est la première femme

-   Nombre de femmes ayant siégé depuis la fondation : 11

-   1983 : Léopold Sédar Senghor (dont je vous invite à aller regarder
    > le travail) est le premier homme noir

-   Nombre de membres depuis la fondation : 733 en 2019

On ne peut pas dire qu'il s'agisse d'un modèle de diversité...

#### Et "le masculin l'emporte sur le féminin" 

Dès la création de l'Académie, elle s'est évertuée à effacer les femmes
de la langue française et notamment de tout ce qui connote au prestige.
Voici un florilège de citations :

-   1651, Liberté de la Langue Françoise, dans sa pureté par Scipion
    > Dupleix (1569-1661), "Conseiller du Roy en ses Conseils d'État &
    > Privé, & et historiographe de France" Parce que le genre masculin
    > est le plus noble, il prevaut tout seul contre deux ou plusieurs
    > feminins, quoy qu'ils soient plus proches de leur adjectif. Par
    > exemple, le travail, la conduite & la fortune joints ensemble
    > peuvent tout.

-   1767, Beauzé (1717-1789) de l'Académie française Le genre masculin
    > est réputé plus noble que le féminin à cause de la supériorité du
    > mâle sur la femelle.

-   8 mai 1673, Bossuet (1627-1704) de l'Académie française a voulu que
    > l'orthographe distingue les gens de lettres d'avec les ignorants
    > et les simples femmes.

![](media/image1.png){width="6.925in" height="1.4805555555555556in"}

#### Invisibilisation

Quoiqu'il y ait un grand nombre de femmes qui professent, qui gravent,
qui composent, qui traduisent, etc., on ne dit pas professeuse,
graveuse, compositrice, traductrice, etc., mais bien professeur,
graveur, compositeur, traducteur, etc., pour la raison que ces mots
n'ont été inventés que pour les hommes qui exercent ces professions.

Voilà ce que l'on retrouve à la page 38 du livre *La Grammaire
nationale, Bescherelle* en 1834. En lisant ce court passage, il m'est
venu une envie de découper mon édition que je garde depuis le collège en
ayant une sensation de trahison ! En effet, pendant longtemps, je me
fiais au Bescherelle\...

Enfin, en 1944, les mots députée, sénatrice, ambassadrice, présidente,
générale, colonelle, magistrate, préfète... sont interdits et purement
interdits au Journal officiel et ce sont les équivalents masculins qui
apparaissent lors de l'enregistrement à la nomination...

### Retour en grâce : chronologie de la législation en faveur de l'écriture inclusive 

-   1984 : Yvette Roudy (ministre du droit de la femme sous F. Mitterand
    > qui est également le premier ministère dédié) crée une commission
    > de terminologie relative à la reféminisation des noms de métier

-   1986 : Signature de la 1re circulaire de reféminisation des noms de
    > métier dans les documents réglementaires ou administratifs. Il y a
    > une opposition de l'Académie française.

-   1998 : Circulaire Jospin - Création d\'une documentation de
    > référence listant le plus grand nombre de noms de métiers au
    > féminin et masculin \"Femme, j'écris ton nom...\"

-   2012 : Circulaire Fillon - Suppression de l'emploi du mademoiselle
    > dans tous les formulaires administratifs

-   2017 : Circulaire Philippe - Elle reconnaît la nécessité de "ne pas
    > marquer de préférence de genre" et préconise l'usage de formules
    > telles que le candidat et la candidate. elle proscrit l'écriture
    > dite inclusive, qui désigne les pratiques rédactionnelles et
    > typographiques visant à substituer à l'emploi du masculin,
    > lorsqu'il est utilisé dans un sens générique, une graphie faisant
    > ressortir l'existence d'une forme féminine" des textes destinés à
    > paraitre au Journal Officiel. Ici, le ministre interdit donc
    > l'usage de parenthèses pourtant largement utilisé dans le service
    > public (pas le pingouin le plus affuté du tiroir)...

-   2019 : l'Académie française n'y voit plus aucun "obstacle de
    > principe"

-   2021 : Circulaire Blanquer - Réaffirmation de la circulaire Philippe

![](media/image2.png){width="4.71875in" height="1.3958333333333333in"}

Si vous vous demandez ce qu'est une circulaire, il s'agit d\'un texte
administratif rédigé pour informer les différents services d'un
ministère, ou les agents des services déconcentrés (préfecture, par
exemple). Ce texte explique les dispositifs à appliquer. Les circulaires
doivent être publiées pour être opposables, c\'est-à-dire qu\'elles
produisent alors leurs effets auprès des personnes concernées qui ne
peuvent l\'ignorer.

Une circulaire émane d\'une autorité publique. C\'est un acte
administratif." [[Pour en savoir plus sur le sujet]{.underline}
![](media/image3.png){width="0.28125in"
height="0.28125in"}](https://www.vie-publique.fr/fiches/20265-quest-ce-quune-circulaire)
