---
author: Cécile Freyd-Foucault
title: L'écriture inclusive, ce n'est pas que le point médian
---

# L\'écriture inclusive, ce n\'est pas que le point médian 

## Partie 1 sur 4

Il y a quelque temps, j'ai commencé à m'intéresser à l'écriture
inclusive, car j'entendais tout et son contraire à ce sujet. J'ai
commencé à lire des articles, des livres, regarder des vidéos, etc. Ma
conclusion a été "ce sujet est passionnant". Mais, j\'ai surtout
découvert l'impact sociétal de l'écriture inclusive.

L'objectif de cet article est de faire un condensé de mes lectures sur
pourquoi ce sujet est important avec une rétrospective historique, les
impacts, comment pratiquer une écriture inclusive et surtout revenir sur
plusieurs idées reçues.

Pour commencer, je vous propose 2 définitions de Raphaël Haddad qui a
beaucoup publié sur le sujet :

L'écriture inclusive n'est pas woke. C'est une tradition française.
Reféminisation des noms de métiers, convention de proximité, double
flexion : elle dépasse largement la question du point médian *L'écriture
inclusive, et si on s'y mettait* aux éditions le Robert).

Cette première citation est intéressante, car elle pointe du doigt le
premier apriori que l'on peut avoir en indiquant les différents moyens
mis à disposition. Il ne s\'agit pas de déformer l'écriture, mais
d'utiliser toute sa richesse.

La seconde définition : L'écriture inclusive peut se définir simplement
comme : un ensemble d'attentions éditoriales qui permettent d'assurer
une égalité de représentations entre les femmes et les hommes (*Manuel
d'écriture inclusive*, Raphaël Haddad, 2016) explicite clairement
l'objectif d'user d'une écriture inclusive : être un des outils de
l'égalité.

### Petit historique

Je vous propose un retour dans le passé, et pas n'importe où, au Moyen-
ge, au XIIIe siècle. À cette époque, le registre d'impôt médiéval dresse
une série de métiers parmi lesquels on trouve mairesse, archière,
cervoisière, portière ou fusicienne ("femme qui exerce la médecine"). Il
reconnaît les chevaleresses, les gouverneresses ou les médecines.

À cette même époque, l'accord de proximité est admis, c'est à dire,
qu'on accorde en genre l'adjectif par rapport au terme le plus
géographiquement proche. On peut se demander aujourd'hui pourquoi on
accepte "des chants et danses bretonnes" et pas \"des hommes et des
femmes bretonnes" ? Enfin, le "e" final n'est pas muet.

Et là, patatra ! En 1634, Richelieu crée l'Académie française. Pour
celleux qui ne la connaissent pas, il s\'agit d'une institution qui a
pour objectif de donner des règles certaines à notre langue et à la
rendre pure, éloquente et capable de traiter les arts et les sciences.
Si vous souhaitez en savoir plus, je vous invite à aller regarder
[cette vidéo](https://www.youtube.com/watch?v=hfUsGmcr1PI&t=126s&ab_channel=Linguisticae")
. J'en profite pour vous donner quelques statistiques sur cette
institution :

-   1980 : Marguerite Yourcenar est la première femme

-   Nombre de femmes ayant siégé depuis la fondation : 11

-   1983 : Léopold Sédar Senghor (dont je vous invite à aller regarder
    > le travail) est le premier homme noir

-   Nombre de membres depuis la fondation : 733 en 2019

On ne peut pas dire qu'il s'agisse d'un modèle de diversité...

#### Et "le masculin l'emporte sur le féminin" 

Dès la création de l'Académie, elle s'est évertuée à effacer les femmes
de la langue française et notamment de tout ce qui connote au prestige.
Voici un florilège de citations :

-   1651, Liberté de la Langue Françoise, dans sa pureté par Scipion
    > Dupleix (1569-1661), "Conseiller du Roy en ses Conseils d'État &
    > Privé, & et historiographe de France" Parce que le genre masculin
    > est le plus noble, il prevaut tout seul contre deux ou plusieurs
    > feminins, quoy qu'ils soient plus proches de leur adjectif. Par
    > exemple, le travail, la conduite & la fortune joints ensemble
    > peuvent tout.

-   1767, Beauzé (1717-1789) de l'Académie française Le genre masculin
    > est réputé plus noble que le féminin à cause de la supériorité du
    > mâle sur la femelle.

-   8 mai 1673, Bossuet (1627-1704) de l'Académie française a voulu que
    > l'orthographe distingue les gens de lettres d'avec les ignorants
    > et les simples femmes.

![](files/media/image1.png)

#### Invisibilisation

Quoiqu'il y ait un grand nombre de femmes qui professent, qui gravent,
qui composent, qui traduisent, etc., on ne dit pas professeuse,
graveuse, compositrice, traductrice, etc., mais bien professeur,
graveur, compositeur, traducteur, etc., pour la raison que ces mots
n'ont été inventés que pour les hommes qui exercent ces professions.

Voilà ce que l'on retrouve à la page 38 du livre *La Grammaire
nationale, Bescherelle* en 1834. En lisant ce court passage, il m'est
venu une envie de découper mon édition que je garde depuis le collège en
ayant une sensation de trahison ! En effet, pendant longtemps, je me
fiais au Bescherelle\...

Enfin, en 1944, les mots députée, sénatrice, ambassadrice, présidente,
générale, colonelle, magistrate, préfète... sont interdits et purement
interdits au Journal officiel et ce sont les équivalents masculins qui
apparaissent lors de l'enregistrement à la nomination...

### Retour en grâce : chronologie de la législation en faveur de l'écriture inclusive 

-   1984 : Yvette Roudy (ministre du droit de la femme sous F. Mitterand
    > qui est également le premier ministère dédié) crée une commission
    > de terminologie relative à la reféminisation des noms de métier

-   1986 : Signature de la 1re circulaire de reféminisation des noms de
    > métier dans les documents réglementaires ou administratifs. Il y a
    > une opposition de l'Académie française.

-   1998 : Circulaire Jospin - Création d\'une documentation de
    > référence listant le plus grand nombre de noms de métiers au
    > féminin et masculin \"Femme, j'écris ton nom...\"

-   2012 : Circulaire Fillon - Suppression de l'emploi du mademoiselle
    > dans tous les formulaires administratifs

-   2017 : Circulaire Philippe - Elle reconnaît la nécessité de "ne pas
    > marquer de préférence de genre" et préconise l'usage de formules
    > telles que le candidat et la candidate. elle proscrit l'écriture
    > dite inclusive, qui désigne les pratiques rédactionnelles et
    > typographiques visant à substituer à l'emploi du masculin,
    > lorsqu'il est utilisé dans un sens générique, une graphie faisant
    > ressortir l'existence d'une forme féminine" des textes destinés à
    > paraitre au Journal Officiel. Ici, le ministre interdit donc
    > l'usage de parenthèses pourtant largement utilisé dans le service
    > public (pas le pingouin le plus affuté du tiroir)...

-   2019 : l'Académie française n'y voit plus aucun "obstacle de
    > principe"

-   2021 : Circulaire Blanquer - Réaffirmation de la circulaire Philippe

![](files/media/image2.png)

Si vous vous demandez ce qu'est une circulaire, il s'agit d\'un texte
administratif rédigé pour informer les différents services d'un
ministère, ou les agents des services déconcentrés (préfecture, par
exemple). Ce texte explique les dispositifs à appliquer. Les circulaires
doivent être publiées pour être opposables, c\'est-à-dire qu\'elles
produisent alors leurs effets auprès des personnes concernées qui ne
peuvent l\'ignorer.

Une circulaire émane d\'une autorité publique. C\'est un acte
administratif." [[Pour en savoir plus sur le sujet]{.underline}
![](files/media/image3.png){width="0.28125in"
height="0.28125in"}](https://www.vie-publique.fr/fiches/20265-quest-ce-quune-circulaire)

### Écriture inclusive : quels impacts ? 

Si l'on parle et promeut l'écriture inclusive, ce n'est pas pour le
plaisir "d'embêter" les personnes (ni de se prendre un certain nombre de
violences quand on l'utilise). Si l'on en parle beaucoup, c'est qu'il y
a un impact réel à son usage.

#### L'écriture inclusive indispensable aux politiques d'égalité 

Dans ces travaux, l'historienne et linguiste Éliane Viennot établit que
la virilisation de la langue est une construction historique et
politique. La règle grammaticale selon laquelle le masculin l'emporte
sur le féminin est avant tout un ordre, au sens d'agencement et
d'autorité, social et politique. Elle explique que le langage est le
moyen et le média au travers duquel nous nous construisons et nous
faisons relation les un·es avec les autres.

"Les inégalités sont le fondement et l'expression d'un système politique
et économique patriarcal."

Ainsi, avoir un langage non sexiste fait partie d'un ensemble plus large
pour compenser et porter les conditions de l'égalité. Si l'écriture
inclusive seule ne suffit pas, elle fait partie des conditions sine qua
non à l'égalité femme homme !

#### Des impacts concrets 

Aujourd'hui, l'écriture dite inclusive s'est beaucoup démocratisée. En
effet, 2 tiers des Français sont favorables à la reféminisation des
métiers et 56 % de la population se déclare attentive à la recherche
d'alternatives au masculin générique. Enfin, les jeunes (18 - 34 ans) et
les femmes se déclarent plus favorables au point médian et aux mots non
binaires que le reste de la population (comme c'est étonnant !). On
commence à mesurer ses impacts.

Le premier concerne notre représentation mentale individuelle. Lors de
l'usage du masculin générique grammatical, nos représentations sont
essentiellement masculines. Au contraire, les formulations fléchies,
épicènes et englobantes créent davantage de représentations mentales
féminines.

![](files/media/image4.png){width="6.854166666666667in"
height="6.854166666666667in"} [[Lire l\'alternative textuelle du
graph]{.underline}](https://cecilefreydf.com/articles/alt_graph_art1.html)

En 2008, dans l'étude "Un ministre peut-il tomber enceinte ?" l'impact
du générique masculin sur les représentations mentales", l'équipe
chercheuse met en en avant qu'en moyenne, "23 % des représentations
mentales sont féminines après l'utilisation d'un générique masculin,
alors que ce même pourcentage est de 43 % après l'utilisation d'un
générique épicène."

Quand on sait qu'en France, il n'y a que 7 % de femmes dirigeantes
d'entreprises de plus de 1000 salarié·es, user davantage de formulation
telle que dirigeantes et dirigeants plutôt que du simple dirigeants pour
parler de chef·fes d\'entreprises aiderait à avoir une meilleure
représentation mentale qui pousserait à davantage de mixité.

Un second impact se passe plus au niveau du recrutement. En effet, les
femmes postulent moins à des annonces qui utilisent le masculin
générique. Suite à la mise en place de l'écriture inclusive en 2016, le
cnam (école d'ingénieur·es) indiquait fin 2018 dénombrer 30 % de
candidatures féminines en plus alors que l'école ne comptait en 2014 que
16 % de femmes diplômées !

Dans son guide à destination des TPE-PME sur l'égalité femmes hommes, le
Laboratoire de l'Égalité recommande écrire clairement « Recherche
Technicien/Technicienne » ou « Directeur/Directrice » plutôt que
d'utiliser le cache-sexe h/f qui met en fait l'annonce au masculin comme
par exemple : « Technicien h/f », « Directeur h/f » .

Enfin, un troisième impact qui n'a pas encore aujourd'hui d'étude
terminée, mais pour lequel les témoignages se multiplient, l'impact sur
l'égalité professionnelle : plus la présence de l'écriture inclusive au
sein d'une entreprise est forte, plus l'égalité professionnelle est
forte ! En effet, il s'agit d'une mesure concrète qui engage les
entreprises dans un vrai changement.

#### "Oui, mais le neutre !" 

Souvent, on parle du neutre pour justifier le non-usage de l'écriture
inclusive. Même si ce neutre peut exister (notamment pour les termes
épicènes), il ne peut pas être porté par le masculin générique qui est
ambigu.

Le chercheur en psycholinguistique à l'Université de Fribourg Pascal
Gygax explique que notre cerveau résout l'ambiguïté sémantique du
masculin aux dépens des femmes et au profit des hommes, systématiquement
et indépendamment du contrôle que nous essayons parfois de mettre en
place. Ceci veut simplement dire que le sens « masculin = homme » est
toujours activé, et que nous ne pouvons pas empêcher cette activation,
même si nous essayons d'activer consciemment les sens « masculin = mixte
ou neutre ».

#### "Pour faciliter la lecture du document, le masculin générique est utilisé pour désigner les deux sexes" 

Très (trop) souvent, nous retrouvons ce type de formulation, et
notamment dans des offres d'emploi.

Dans un souci d\'accessibilité et de clarté, le point médian n\'est pas
utilisé dans cette annonce. Tous les termes employés se réfèrent aussi
bien au genre féminin que masculin. ou encore Quand \[nom de
l'entreprise\] dit \"développeur\" \[nom de l'entreprise\] s\'adresse à
toute personne humaine qui développe (\...)

Il y a plusieurs problèmes à ce type de formulation, la première est que
le masculin générique fait et fera toujours référence à l'homme. Et
l'ajout de ce genre de petites phrases n'y changera rien comme le montre
encore Pascal Gygax dans un entretien : Nous avons montré que le sens «
masculin = homme » est activé de manière passive, non consciente et
incontrôlable. Nous appelons ceci un mécanisme de résonance. La mention
dont vous parlez ne changera rien à cette activation.

Ensuite, car elle instrumentalise l'accessibilité (ça sera le sujet du
troisième article de cette série), sans pour autant chercher à faire
évoluer sa pratique de l'écriture en se servant des nombreux outils mis
à notre disposition.

À vous qui cherchez à recruter, voici 2 réactions de femmes à qui j'ai
fait lire une annonce où la phrase "Dans un souci d\'accessibilité et de
clarté, le point médian n\'est pas utilisé dans cette annonce. Tous les
termes employés se réfèrent aussi bien au genre féminin que masculin"
apparaissait. Voyez par vous-même l'effet obtenu !

"Perso, quand je lis ce genre de phrase, je comprends \"je sais que des
gens réclament que ça soit plus inclusif, mais je n'en ai rien à faire,
faites l\'effort vous-même de vous inclure"

Quand je vois ce type de texte, je fume, car c\'est opposer
accessibilité et féminisme, comme si c\'était 2 combats distincts et
opposables. Choisis ton camp : tu soutiens les personnes en situation de
handicap, ou les femmes ? Je rage, car trop souvent j\'entends \"c\'est
à cause de l\'accessibilité\...\" et ce n\'est pas vrai 99% du temps. Ce
sont des excuses pour ne pas se donner la peine de creuser le sujet.
Oui, avec des termes épicènes, ça fonctionne, oui, en doublant les
termes ça fonctionne aussi."

C'est terminé pour cette première partie. Dans la suite, nous verrons
comment concrètement se pratique l'écriture inclusive. À très vite !

[Cécile Freyd-Foucault](https://twitter.com/cecilefreydf)
