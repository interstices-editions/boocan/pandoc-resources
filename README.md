# Pandoc en quelques commandes

## Conversion d' un `docx` en `markdown`

### Conversion

```bash
pandoc article.docx -t markdown
```

Affiche la version markdown.

---

```bash
pandoc article.docx -t html 
```

Affiche la version html

---

L'option `-s` pour `--standalone`

```bash
pandoc article.docx -s -t html
```

Affiche la version html, page complète avec `<head>`, `<body>`…

On remarque également l'avertissement suivant en début de sortie:

```
[WARNING] This document format requires a nonempty <title> element.
  Defaulting to 'article' as the title.
  To specify a title, use 'title' in metadata or --metadata title="...".
```

### Métadonnées


```bash
pandoc article.docx -s -t html --metadata title="L'écriture inclusive, ce n'est pas que le point médian"
```

```bash
pandoc article.docx -s -t markdown --metadata title="L'écriture inclusive, ce n'est pas que le point médian"
```

La version markdown débute maintenant avec un bloc de métadonnées

```
---
title: L'écriture inclusive, ce n'est pas que le point médian
---
```

Si l'on souhaite renseigner plusieurs métadonnées, il est plus simple de les conserver dans un fichier `yaml`.

```bash
pandoc article.docx -s -t markdown --metadata-file=article.yml
```

### Enregsitrer la sortie dans un fichier

Très simple avec le `>` qui permet de rediriger la sortie vers un fichier.

```bash
pandoc article.docx -s -t markdown --metadata-file=article.yml > article.md
```

Pandoc fournit également un moyen simple et concis avec l'option `-o`:

```bash
pandoc article.docx -s --metadata-file=article.yml -o article.md
```

Le format de sortie est fonction de l'extension du fichier cible, ici `.md`, pour `markdown`.


## Travailler avec plusieurs fichiers

Quand il s'agit de longs documents, il peut-être utile de scinder le texte en plusieurs parties.

Pandoc supporte plusieurs fichiers en entrée:

```bash
pandoc 01-article.md 02-article.md -s --metadata-file=article.yml -o article.pdf
```

## Extraire les media du document source

Utiliser l'option `--extract-media=DIR`. Par exemple, pour les extraire dans un dossier `files/` dans le dossier en cours:

```bash
pandoc article.docx -s --metadata-file=article.yml --extract-media=files -o article.md
```




## Ressources

- [Exemples de commandes Pandoc](https://pandoc.org/demos.html)
- [Getting started with pandoc](https://pandoc.org/getting-started.html)
- [Pandoc - Options](https://pandoc.org/MANUAL.html#options)

